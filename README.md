MongoDB NodeJS Driver: https://mongodb.github.io/node-mongodb-native/3.5/api/

# TODOS

Ver sobre licenças.

Pôr um esquema para viewmodel. (Focar para microserviços - apenas endpoints -, por enquanto)

Pôr um chat, conversa em tempo real.

Ver como ficarão os one-to-many, many-to-many e one-to-one. Rever um pouco o módulo do banco de dados, não tá
100% ainda. Pesquisar o que são os indexes do MongoDB.

Falta terminar a documentação do database e fazer a do controllers_loader.

Escrever a documentação do módulo de autenticação.

Criar uma forma de especificar rotas de uma forma global, como:
* resource(urlInCommon, controllerFile)
que vai criar as respectivas rotas de busca, criação, atualização, etc, usando nomes padrão de funções (update, show, delete, etc)

Talvez passar os middlewares do `route.group` por um `.middleware` depois. https://adonisjs.com/docs/4.1/routing#_middleware

Ver a possibilidade de pôr um `.namespace` no `route.group` para facilitar adição de novas rotas ao mesmo namespace.