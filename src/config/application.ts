const expiresIn: string = '1d', algorithm: 'RS256' = 'RS256';

/**
 * List here the variables that must be filtered from the logging
 */
const filterOut = ['password'];

/**
 * If you need to do something to the params before processing them,
 * like decrypting them.
 * But, the params listed above will still be filtered out.
 */
function manipulateParams(params: Object) {
  return params;
}

/**
 * JWT Sign Options to create the token
 */
const jwtSignOptions = {
  expiresIn,
  algorithm
}

/**
 * JWT Verify Options to check if the token is valid and retrieve the info.
 * The algorithm may be an array
 */
const jwtVerifyOptions = {
  expiresIn,
  algorithms: [algorithm]
}

/**
 * Application Mailer general config
 */
const mailerConfig = {
  from: '"You" <your@email.com.br>',
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: 'your@email.com.br',
    pass: 'yourpassword'
  },
  devSend: false
}

const application = {
  filterOut,
  manipulateParams,
  jwtSignOptions,
  jwtVerifyOptions,
  mailerConfig,
}

export default application;