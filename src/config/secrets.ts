import * as fs from 'fs';
import * as path from 'path';

const JWT_SECRET_KEY = fs.readFileSync(path.join(__dirname, './keys/jwt-private.key'), 'utf8');
const JWT_PUBLIC_KEY = fs.readFileSync(path.join(__dirname, './keys/jwt-public.key'), 'utf8');

const secrets: Object = {
  JWT_SECRET_KEY,
  JWT_PUBLIC_KEY
}

export default secrets