import environment from './environments';

const db: Object = {
  development: {
    url: 'mongodb://localhost:27017'
  },
  homolog: {
    url: ''
  },
  production: {
    url: ''
  }
}

export default db[environment.getEnv()];