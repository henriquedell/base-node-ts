/**
 * Environment configuration
 */

const environments: Object = {
  development: {
    port: 3000
  },

  homolog: {
    port: 8000
  },

  production: {
    port: 8000
  }
};

const chosenEnvironment = process.env.NODE_ENV == 'production' ? 'production' : (process.env.NODE_ENV == 'homolog' ? 'homolog' : 'development');
environments[chosenEnvironment]['getEnv'] = () => chosenEnvironment;

export default environments[chosenEnvironment];