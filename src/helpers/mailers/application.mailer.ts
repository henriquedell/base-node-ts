function signup(user: Object) {
  Mailer.send({
    to: user.email,
    subject: 'Cadastro efetuado!',
    html: 'signup',
    locals: user
  }).catch((err: any) => console.log(err));
}

const applicationMailer: Object = {
  signup
}; 

export default applicationMailer;