import { readdirSync, lstatSync, PathLike } from "fs";
import { join } from "path";

const capitalize = (s: string) => {
  if (typeof s !== 'string') return '';
  let capitalized = '';
  s.split('_').forEach(part => {
    capitalized += part.charAt(0).toUpperCase() + part.slice(1)
  })
  return capitalized;
}

let models: Object = {};
const modelsDirectory = join(__dirname, `../../../app/models`);

function loadDirectory(modelsDirectory: any) {
  const files = readdirSync(modelsDirectory);
  files.forEach((file, index) => {
    models[file.replace(/[$\.]ts|[$\.]js/, '').split(/\./g).map(partOfName => capitalize(partOfName)).join('')] = require(`${modelsDirectory}/${file.replace(/[$\.]ts|[$\.]js/, '')}`);
  });
}

export default function loadModels() {
  loadDirectory(modelsDirectory);
  return models;
}
