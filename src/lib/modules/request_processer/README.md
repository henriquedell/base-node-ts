# Request Processer

The brain of the framework haha. It is the main function that runs for every request that comes in.

The Request object, req, exposes a lot of useful information about the request through a paramter: `data`.

The Response object, res, has a `.status(number)` to set the HTTP status of the response, as well as a `.json(object)` that accepts an object and send it as a JSON response.

This function logs every step of the process:
* The request that it has started to process
* Which controller was the chosen one to handle it, or a warning of "Not found" if none is found
* The parameters, if there's some
* And some infos about the response

# Auth

For authentication porpuses, on the Request object, there's:
* a `.login(user)` method, that uses the Auth module `signIn` method
* a `.logout()` method, which uses the `logOut` method from Auth module
* a `.verify(token)` method, which uses the `verify` method, from Auth module as well, and retrieve the content from the token