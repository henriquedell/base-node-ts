import { parse } from "url";
import { StringDecoder } from "string_decoder";
import logs from "../logs";
import environment from '@config/environments';
import application from '@config/application';
import { Request, Response } from '../classes';

const prevLogFunction = console.log;

function customizeConsoleLogging() {
  // Create local variable which is bound to the log function of console.
  // var olog = prevLogFunction.bind(this);  
  
  // Override the log method (proxy the log method).  
  // This is the code which captures the console.log.  
  // All console.log will be passing through this log function
  console.log = function(this: Request) {
    // Loop through all arguments to console.log
    for(var i = 0; i < arguments.length; i++){   
      if(this.data) {
        if(typeof(arguments[i]) == 'string' && (arguments[i].includes(this.data.id) || arguments[i].includes('MongoDB'))) {
          prevLogFunction(arguments[i]);
        } else {
          if(environment.getEnv() == 'development') {
            prevLogFunction(logs.logWarningDatesMessages(new Date()), `[${this.data.id}]`, arguments[i]);
          } else {
            prevLogFunction(`[${this.data.id}]`, arguments[i]);
          }
        }
      } else {
        prevLogFunction(arguments[i]);
      }
    }
  }
}

export default function(req: any, res: any) {   
  res.req = req;
  req.res = res;

  req.login = function(user: any, callback: Function) {
    try {
      Auth.signIn(user, this);
      callback(false);
    } catch(e) {
      callback(e);
    }
  }

  req.logout = function() {
    Auth.logOut(this);
  }

  req.verify = function(token: string) {
    return Auth.verify(token);
  }

  req.retrieveUserID = function(callback: Function) {
    Auth.retrieveUserID.bind(req)(callback);
  }
   
  // Get when the request was recieved
  const date = new Date();

  // Get the HTTP Headers
  // const headers = req.headers;

  // Get the IP Address
  const ip = req.connection.remoteAddress;

  // Get the URL
  const parsedUrl = parse(req.url, true);

  // Get the HTTP style
  const protocol = parsedUrl.protocol == null ? 'HTTP' : parsedUrl.protocol;

  // Get the HTTP Verb
  let method = req.method.toUpperCase();

  // Get the Origin
  const origin = parsedUrl.host == null ? 'localhost' : parsedUrl.host;

  // Get the endpoint
  const path = parsedUrl.pathname != null ? parsedUrl.pathname.replace(/^\/+|\/+$/g, '') : '';

  // Get the Query
  const query = parsedUrl.query;

  // Get the Payload - Body -, if there's any
  const paramsDecoder = new StringDecoder('utf-8');
  let paramsAsString = '';
  req.on('data', (data: any) => {
    if (method != 'GET') paramsAsString += paramsDecoder.write(data);
  });

  // Add some helper functions to the Response object
  res['status'] = function(httpCode: number) {
    this.statusCode = httpCode;
    return this;
  }

  res['json'] = function(body = {}) {
    const chunk = this.statusCode == 404 && Helper.isObjectEmpty(body) ? { message: 'Not Found' } : body
    const bodyString = JSON.stringify(chunk);
    this.header('Content-Type', 'application/json');
    this.header('authorization', this.req.headers['authorization'] || null);
    this.end(bodyString);
    if (this.statusCode >= 400) {
      logs.error(this, Helper.isObjectEmpty(body) ? { message: 'Not Found' } : body);
    }
    logs.finishedProcessing(this);
    return this;
  }
  
  res['header'] = function(header: string, content: string) {
    this.setHeader(header, content);
    return this;
  }

  // When the request is fully received
  req.on('end', () => {
    paramsAsString += paramsDecoder.end();

    // Convert the params to a JSON
    let params;
    if (paramsAsString != '') {
      params = JSON.parse(paramsAsString);
    } else {
      params = {};
    }

    // Attach the query to the params object
    for (const entry in query) {
      params[entry] = query[entry];
    }

    /**
     *  Check if there's any _method param to update the
     *  HTTP Method of the request
     */
    for (const param in params) {
      if (param == '_method' && method == 'POST') method = params[param].toUpperCase();
    }

    delete params['_method'];

    // Generate request ID
    let id = Helper.UUID();
    req.headers['x-request-id'] = id;
    res.header('x-request-id', id);

    // Build the Data object to send to the controller
    const data: Object = {
      id,
      date,
      protocol,
      method,
      origin,
      path,
      ip
    }

    // Add the parsed infos of the request to the request
    req.data = data;

    // Attach the data object onto the req object
    for (const key in data) {
      req[key] = data[key];
    }

    // Add the object that will hold URL Params
    req.urlParams = {};

    // Append the params object
    if(application.manipulateParams) {
      params = application.manipulateParams(params);
    }
    req.params = params;

    // Get the right controller
    const controllerExists = Router.routeExists(`${method}:/${path}`, req);
    const chosenRoute = Router.getRoute(`${method}:/${path}`, req);
    const controllerChosen = chosenRoute.controllerChosen;

    // Append controller
    req.controllerChosen = controllerChosen;

    customizeConsoleLogging();

    // Logs the request that is been received
    logs.entryRequest(req);

    // Logs the processing and run the route
    controllerExists ? logs.processingByControler(req) : logs.controllerNotFound(req);
    if (controllerExists && !Helper.isObjectEmpty(req.params)) logs.displayParameters(req);
    chosenRoute.routingFunction(req, res);
    // const result = chosenRoute.routingFunction(req, res);
    // if (result && !(result instanceof ServerResponse)) res.status(result.status || 200).json(result.response);
    // logs.finishedProcessing(res);
  });
}