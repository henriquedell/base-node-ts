# Requests

This module exposes some functions the handles GET and POST requests that the application can perform.

The `get(url, options) => Promise<Response | Error>` function expects the URL to send the request, as well as a HTTP Options object, and returns a Promise with the response data or an error, if one has ocurred.

The `post(url, body, options) => Promise<Response | Error>` function expects the URL, as well as the payload object and a HTTP Options object, and returns a Promise with the response or the error, if some has ocurred.