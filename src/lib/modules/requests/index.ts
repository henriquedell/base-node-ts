import * as http from "http";
import * as https from "https";
import { StringDecoder } from "string_decoder";

const paramsDecoder = new StringDecoder('utf-8');

const get = (url: string, options = {
  protocol: url.split('//')[0],
  headers: {
    'Content-Type': 'application/json'
  }
}) => {
  return new Promise((resolve, reject) => {
    http.get(url, options, res => {
      const { statusCode } = res;
      let body = '';
      res.on('data', data => {
        body += paramsDecoder.write(data);
      });
      res.on('end', () => {
        body += paramsDecoder.end();
        if(statusCode == 200) {
          return resolve(JSON.parse(body));
        } else {
          return reject(JSON.parse(body));
        }
      });
    }).on('error', err => {
      return reject(err);
    });
  });
}

const post = (url: string, body: Object, options = {
  protocol: url.split('//')[0],
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  }
}) => {
  return new Promise((resolve, reject) => {
    const req = http.request(url, options, res => {
      let body = '';
      res.on('data', data => {
        body += paramsDecoder.write(data);
      });
      res.on('end', () => {
        body += paramsDecoder.end();
        return resolve(JSON.parse(body));
      });
      return }).on('error', err => {
      return reject(err);
    });
    req.write(JSON.stringify(body));
    req.end();
  });
}

const requests: Object = {
  get,
  post
}

export default requests;