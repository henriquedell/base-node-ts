# Helpers

This module exports ome useful function for a variety of use cases, like
* `isObjectEmpty(object)`, returns a boolean telling if the object passed as param is empty ot not
* `stringToISOStringDate(date)`, accepts a date in string on the form of DD/MM/YYYY and returns it in ISO format, YYYY-MM-DD
* `status(code)`, accepts a number as param to set as the Status Code of the HTTP Response, this method is attached on the Response object
* `json(object)`, this accepts an object as param and will set it as the response as well as the Content-Type header to application/json and finalize the processing of the request
* `hash(value)`, expects some value to hash it and return the hashed version
* `generateToken()`, returns a 55 multicharacter long token