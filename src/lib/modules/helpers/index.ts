import { randomBytes } from 'crypto';
import * as bcrypt from 'bcrypt';

/**
 * Some functions to extend objects funcionalities
 * and vairous tasks
 */

function isObjectEmpty(object: Object) {
  for (const key in object) {
    if (object.hasOwnProperty(key)) return false;
  }
  return true;
}

function stringToISOStringDate(date: string) {
  let [ dia, mes, ano ] = date.split('/');
  return `${ano}-${mes}-${dia}`;
}

async function hash(value: string) {
  return await bcrypt.hash(value, 8);
}

async function isValid(plainText: string, hash: string) {
  return await bcrypt.compare(plainText, hash);
}

function generateToken() {
  const token = randomBytes(55).toString('hex');
  return token;
}

function UUID(){
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (dt + Math.random()*16)%16 | 0;
    dt = Math.floor(dt/16);
    return (c == 'x' ? r :(r&0x3|0x8)).toString(16);
  });
  return uuid;
}

/**
 * Exporting the data
 */

export {
  hash,
  generateToken,
  isObjectEmpty,
  stringToISOStringDate,
  isValid,
  UUID
}