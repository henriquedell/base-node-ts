import { Request, Response } from '../classes';

import loadControllers from "../controllers_loader";
import loadMiddlewares from "../middlewares_loader";

const loadedControllers: Object = loadControllers();
const loadedMiddlewares: Object = loadMiddlewares();

const routes: Object = {};
let middlewares: string[] = [];

/**
 * HTTP Methods accepted
 */

function addRoute(type: string, endpoint: string, ...handlers: Array<Function | string>) {
  const controller = handlers[handlers.length - 1];
  handlers.pop();
  handlers = [...middlewares, ...handlers];
  routes[`${type}:${endpoint}`] = {
    controllerChosen: controller,
    endpointMiddlewares: handlers.map(handler => {
      if (typeof(handler) == 'string') {
        const middlewareName = handler.split('.')[0];
        const middlewareFunc = handler.split('.')[1];
        if (loadedMiddlewares[`${middlewareName}Middleware`]) {
          const middleware = loadedMiddlewares[`${handler}Middleware`];
          if (middlewareFunc && middlewareFunc != '') {
            return middleware[middlewareFunc];
          } else {
            return middleware.default;
          }
        }
      } else {
        return handler;
      }
    }),
    routingFunction: function(req: Request, res: Response) {
      if (typeof(controller) == 'string') {
        const requestedController = `${controller.split('.')[0]}Controller`;
        const requestedAction = controller.split('.')[1];
        let requestedControllerFile = loadedControllers[requestedController];

        for(const key in loadedControllers) {
          if(endpoint.indexOf(key.toLowerCase()) > -1) {
            requestedControllerFile = loadedControllers[key][requestedController];
          }
        }
        if (requestedControllerFile) {
          import(requestedControllerFile).then(requestedEndpoint => {
            function runEndpoint() {
              if (requestedEndpoint && requestedEndpoint[requestedAction]) {
                return requestedEndpoint[requestedAction](req, res);
              } else if (requestedEndpoint.default[requestedAction]){
                return requestedEndpoint.default[requestedAction](req, res);
              } else {
                return res.status(404).json({message: 'Action not found'});
              }
            }
  
            const middlewaresStack = this.endpointMiddlewares.reduceRight((next: Function, middleware: Function) => async (fn: Function) => {
              try {
                await middleware(req, res, next);
              } catch(e) {
                console.log.bind(req)(e);
                res.status(500).json({ message: 'Internal Error' });
              }
            }, async () => {
              try {
                await runEndpoint();
              } catch(e) {
                console.log.bind(req)(e);
                res.status(500).json({ message: 'Internal Error' });
              }
            });
            return middlewaresStack();
          });
        } else {
          return res.status(404).json({message: 'Controller not found'});
        }
      } else {
        if (controller) return controller(req, res);
        else return res.status(404).json({message: 'Controller not found'});
      }
    },
  }
}

function get(endpoint: string, ...handlers:  Array<Function | string>) {
  addRoute('GET', endpoint, ...handlers);
}

function post(endpoint: string, ...handlers:  Array<Function | string>) {
  addRoute('POST', endpoint, ...handlers);
}

function put(endpoint: string, ...handlers:  Array<Function | string>) {
  addRoute('PUT', endpoint, ...handlers);
}

function patch(endpoint: string, ...handlers:  Array<Function | string>) {
  addRoute('PATCH', endpoint, ...handlers);
}

function del(endpoint: string, ...handlers:  Array<Function | string>) {
  addRoute('DELETE', endpoint, ...handlers);
}

/**
 * Function to retrieve the requested route
 */

function checkEachPartOfRoute(req: Request | null, partOfRoute: string, partOfEndpointRequested: string) {
  if (partOfRoute.startsWith(':')) {
    if (req !== null) {
      // Attach the URL param to the urlParams attribute
      req.urlParams[partOfRoute.replace(/:/, '')] = partOfEndpointRequested;
    }
    return true;
  } else if(partOfRoute == partOfEndpointRequested) {
    return true;
  } else {
    return false;
  }
}

function getRoute(endpoint: string, req: Request) {
  if (routes[endpoint] && endpoint.split(':')[0] == req.method) {
    return routes[endpoint];
  } else {
    let routeFound;
    for(const route in routes) {
      const routeSplitted = route.split('/');
      const endpointSplitted = endpoint.split('/');
      if (routeSplitted.length == endpointSplitted.length) {
        for (let i = 0; i < routeSplitted.length; i++) {
          const tested = checkEachPartOfRoute(req, routeSplitted[i], endpointSplitted[i]);
          if(!tested) {
            break;
          } else if(i == routeSplitted.length - 1) {
            routeFound = route;
          }
        }
      }
    }
    return routeFound ? (endpoint.split(':')[0] == req.method ? routes[routeFound] : routes['GET:notFound']) : routes['GET:notFound'];
  }
}

/**
 * Function that checks if the requested route exists
 */

function routeExists(endpoint: string, req: Request) {
  if (routes[endpoint]) {
    if (endpoint.split(':')[0] == req.method) {
      return true;
    } else {
      return false;
    }
  } else {
    let routeFound;
    for(const route in routes) {
      const routeSplitted = route.split('/');
      const endpointSplitted = endpoint.split('/');
      if (routeSplitted.length == endpointSplitted.length) {
        for (let i = 0; i < routeSplitted.length; i++) {
          const tested = checkEachPartOfRoute(null, routeSplitted[i], endpointSplitted[i]);
          if(!tested) {
            break;
          } else if(i == routeSplitted.length - 1) {
            routeFound = route;
          }
        }
      }
    };
    return routeFound && endpoint.split(':')[0] == req.method ? true : false;
  };
}

/**
 * Auto-route if no other is found
 */

get('notFound', (req: Request, res: Response) => {
  res.status(404).json({ message: `${req.method} /${req.path} Not Found` });
});

/**
 * Functions to manipulate the middlewares pipeline
 */

function use(...middlewareList: string[]) {
  middlewares = middlewares.concat(middlewareList);
}

function group(endpoints: Function, ...middlewareList: string[]) {
  const prevLastIndex = middlewares.length - 1;
  middlewares = middlewares.concat(middlewareList);
  endpoints();
  middlewares.splice(prevLastIndex + 1, middlewareList.length);
}

/**
 * Exporting the data
 */

const router = {
  get,
  post,
  put,
  patch,
  delete: del,
  getRoute,
  routeExists,
  routes,
  use,
  group
}

export default router;