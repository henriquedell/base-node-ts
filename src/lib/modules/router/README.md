# Router

This module contains all functions needed to make a perfectly working router.

It exposes the five main HTTP verbs as methods to create those endpoints:
* get(endpoint, controller)
* post(endpoint, controller)
* put(endpoint, controller)
* patch(endpoint, controller)
* del(endpoint, controller) -> For DELETE requests
All of them expects two parameters, a string containing the endpoint, like `/home`, or even `/home/:id` because it supports
URL parameters, and a string of the Controller.action that will handle the request, like `HomeController.index`.

It also exposes two more functions, `getRoute` and `routeExists`, that expects two parameters as well:
* A string containing the HTTP verb, a colon and the path, like `GET:/home`
* The request object

It also exposes the `routes` object that holds every route loaded on the application and its specification, like:
```
    controllerChosen: String,
    routingFunction: Function => Promise<undefined | error>
```

All Query String and payload, if the HTTP verb is other than GET, params are grouped into a `params` attribute inside the req.data object. The URL params are grouped inside a `urlParams` attribute.