import * as nodemailer from 'nodemailer';
import * as Email from 'email-templates';
import { join } from 'path';
import application from '@config/application';

function send(
  data: Object,
  config: Object = {}
) {
  return new Promise((resolve, reject) => {
    /**
     * Creates the Nodemailer's Transporter configuration object
     */
    const transporterConfig: Object = Object.assign({}, application.mailerConfig);
    if (!Helper.isObjectEmpty(config)) for (const key in config) {
      transporterConfig[key] = config[key];
    }

    if (data.from) {
      transporterConfig.from = data.from;
      delete data.from;
    }

    /**
     * Variables that can be accessed in the template
     */
    const locals = data.locals;
    delete data.locals;

    /**
     * Instantiate the Nodemailer's Transporter
     */
    const transporter = nodemailer.createTransport(transporterConfig);

    if(data.html) {
      const file = data.html;
      delete data.html;
      delete data.text;
      /**
       * This is used when sending an email
       * that has an HTML template file
       */
      const email = new Email({
        message: {
          from: transporterConfig.from
        },

        /**
         * To send the email on development mode, set this flag to true.
         */
        send: transporterConfig.devSend,

        transport: transporter,
        views: {
          options: {
            extension: 'ejs',
          },
        },
        getPath: (type, template, locals) => {
          return join(__dirname, `../../../app/emails/${template}.ejs`);
        }
      })

      email.send({
        template: file,
        message: data,
        locals
      }).then(_ => {
        console.log(`Mail sent to ${data.to}`)
        resolve()
      })
      .catch(err => reject(err));
    } else {
      transporter.sendMail(data)
      .then(_ => {
        console.log(`Mail sent to ${data.to}`)
        resolve()
      })
      .catch(err => reject(err));
    }
  });
}

const mailer: Object = {
  send
}

export default mailer;