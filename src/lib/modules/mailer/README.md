# Mailer

This module is used to send emails. It has one function named `send` that takes up to two parameters:
* The first is a Data object
* the second, a Config object

The Config object is optional, it defaults to the `mailerConfig` object set in the `config/application.js`.
It can receive the same attributes used in the Nodemailer's `createTransport` function.
Beyong that, it can be passed a `devSend` boolean that tells if the module should send the email even on Development environment. **But its only used in the case of sending an HTML file**.
If its just text, this boolean isn't used.

**_Please, set the `from` attribute in the Config instead of the Data object_**

The Data object can holds the same parameters as the Nodemailer's Tranporter `sendMail` function, with two differences, the `html` param and a `locals` param:
* `html`: you need to pass a HTML file name, **without the extension**, that lives inside the `src/app/email/` and is written in **EJS**
* `locals`: is an object containing all the variables you want to access inside the HTML template