import { IncomingMessage, ServerResponse } from "http";

declare class Request extends IncomingMessage {
  res: Response;
  urlParams: Object;
  params: Object;
  path: string;
  login: (user: any, callback: Function) => void;
  logout: () => void;
  verify: (token: string) => string | object | undefined;
  retrieveUserID: (callback: Function) => void;
  data: Object;
  id: string;
  protocol: string;
  origin: string;
  date: Date;
  controllerChosen: string | Function;
  currentUser: Object;
}

declare class Response extends ServerResponse {
  req: Request;
  header: (header: string, content: string) => this;
  status: (httpCode: number) => this;
  json: (body: Object) => this;
}

export {
  Request,
  Response
}