import { createServer, IncomingMessage, ServerResponse } from 'http';
import requestProcesser from '../request_processer';
import environment  from'@config/environments';
import logs from '../logs';

import '../globals';

export default class Server {
  listen() {

    createServer((req: IncomingMessage, res: ServerResponse) => {

      // Main function that process all requests
      requestProcesser(req, res);
      
    }).listen(environment.port, () => {
      logs.serverStarted(environment);
    });
  }

  include(moduleToInclude: Function) {
    moduleToInclude();
  }
}