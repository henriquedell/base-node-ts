# Server

This module instantiate a HTTP server using the `config/environments.js` preferences;
Pass the `request_processer` function to handle all requests that comes in;
And log when ths server is up and running.

It also exposes a function through the Server object called
```
    include(moduleToInclude)
```
that runs the `moduleToInclude` to make it available on the application.