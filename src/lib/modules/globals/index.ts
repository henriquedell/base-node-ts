import router from '../router';
import requests from '../requests';
import mailer from '../mailer';
import * as helper from '../helpers';
import auth from '../auth';

declare var global: Global;

global.Router = router;
global.Requests = requests;
global.Mailer = mailer;
global.Helper = helper;
global.Auth = auth;