import environment from '@config/environments';
import application from '@config/application';
import * as fs from 'fs';
import * as path from 'path';
import { Request, Response } from "../classes";

const loggingDir = path.join(__dirname, '../../../../.logs');
const loggingFile = `${loggingDir}/${environment.getEnv()}.log`;

/**
 * Some append log functions to files
 */
if(environment.getEnv() == 'development') {
  if(!fs.existsSync(loggingDir)) {
    fs.mkdirSync(loggingDir);
  }
}

if(fs.existsSync(loggingFile)) {
  const { birthtime } = fs.statSync(loggingFile);
  const now = new Date();
  
  // 3600000 milliseconds == 1 hour
  const day = 24 * 3600000;
  if (now.getTime() - birthtime.getTime() > day) {
    fs.unlinkSync(loggingFile);
  }
}

function logToFile(s: string) {
  fs.open(loggingFile, 'a', function(err, file) {
    if(!err && file) {
      fs.appendFile(file, `${s}\n`, function(err) {
        if(!err) {
          fs.close(file, function(err) {
            if(err) {
              console.log('Unable to close logging file');
            }
          });
        } else {
          console.log('Unable to append data to logging file');
        }
      });
    } else {
      console.log('Unable to open logging file');
    }
  });
}

/**
 * Some useful console colors
 */
const colors = {
  Reset: "\x1b[0m",
  Bright: "\x1b[1m",
  Dim: "\x1b[2m",
  Underscore: "\x1b[4m",
  Blink: "\x1b[5m",
  Reverse: "\x1b[7m",
  Hidden: "\x1b[8m",

  FgBlack: "\x1b[30m",
  FgRed: "\x1b[31m",
  FgGreen: "\x1b[32m",
  FgYellow: "\x1b[33m",
  FgBlue: "\x1b[34m",
  FgMagenta: "\x1b[35m",
  FgCyan: "\x1b[36m",
  FgWhite: "\x1b[37m",

  BgBlack: "\x1b[40m",
  BgRed: "\x1b[41m",
  BgGreen: "\x1b[42m",
  BgYellow: "\x1b[43m",
  BgBlue: "\x1b[44m",
  BgMagenta: "\x1b[45m",
  BgCyan: "\x1b[46m",
  BgWhite: "\x1b[47m"
}

function filterSensibleValues(json: Object) {
  const newJson: Object = {};
  for (const key in json) {
    if (application.filterOut.filter(filteredAttr => filteredAttr == key).length > 0) {
      newJson[key] = "[FILTERED]";
    } else {
      if (typeof(json[key]) == 'object') {
        newJson[key] = filterSensibleValues(json[key]);
      } else {
        newJson[key] = json[key];
      }
    }
  }
  return newJson;
}

// const logWarningDatesMessages = function(date) {
//   return `[${date.toDateString()} ${date.toTimeString()}]`
// }

/**
 * Some logging functions to make the system ready to log each part of the process
 */
const logs = {
  logWarningDatesMessages: function(date: Date) {
    return `[${date.toDateString()} ${date.toTimeString()}]`
  },
  dbConnected: () => {
    const date = new Date();
    console.log(`${colors.FgGreen}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Server:${colors.Reset} Connected to the Database`);
    logToFile(`${logs.logWarningDatesMessages(date)} Server: Connected to the Database`);
  },
  serverStarted: (env: Object) => {
    const date = new Date();
    console.log(`${colors.FgGreen}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Server:${colors.Reset} Server is UP, port: ${env.port}, mode: ${env.getEnv().toUpperCase()}`);
    logToFile(`${logs.logWarningDatesMessages(date)} Server: Server is UP, port: ${env.port}, mode: ${env.getEnv().toUpperCase()}`);
  },
  entryRequest: (req: Request) => {
    console.log();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(req.date) : ''} Router Started:${colors.Reset}${colors.FgWhite} [${req.id}] Started ${req.protocol} ${req.method} "/${req.path}" for ${req.origin} at ${req.date}${colors.Reset}`);
    logToFile(`${logs.logWarningDatesMessages(req.date)} Router Started: [${req.id}] ${req.protocol} ${req.method} "/${req.path}" for ${req.origin} at ${req.date}`);
  },
  processingByControler: (req: Request) => {
    let controller;
    if (typeof(req.controllerChosen) == 'function') {
      controller = req.controllerChosen.name;
    } else if (typeof(req.controllerChosen) == 'string') {
      controller = req.controllerChosen;
    }

    if (controller) {
      const date = new Date();
      console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Router Working:${colors.Reset} ${colors.FgWhite}[${req.id}] Processing by controller ${colors.Bright}${controller}()${colors.Reset}`);
      logToFile(`${logs.logWarningDatesMessages(date)} Router Working: [${req.id}] Processing by ${controller}()`);
    }
  },
  displayParameters: (req: Request) => {
    const date = new Date();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Router Working:${colors.Reset} ${colors.FgWhite}[${req.id}] Parameters: ${JSON.stringify(filterSensibleValues(req.params))}${colors.Reset}`);
    logToFile(`${logs.logWarningDatesMessages(date)} Router Working: [${req.id}] Parameters => ${JSON.stringify(filterSensibleValues(req.params))}`);
  },
  controllerNotFound: (req: Request) => {
    const date = new Date();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Router Working:${colors.Reset} ${colors.FgWhite}[${req.id}] The controller you were requesting was not found${colors.Reset}`);
    logToFile(`${logs.logWarningDatesMessages(date)} Router Working: [${req.id}] The controller you were requesting was not found`);
  },
  
  finishedProcessing: (res: Response) => {
    const date = new Date();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Router Ended:${colors.Reset} ${colors.FgWhite}[${res.req.id}] Completed ${res.req.protocol} ${res.req.method} "/${res.req.path}" with ${res.statusCode} ${res.statusMessage} in ${date.getMilliseconds() - res.req.date.getMilliseconds()}ms${colors.Reset}`);
    logToFile(`${logs.logWarningDatesMessages(date)} Router Ended: [${res.req.id}] Completed ${res.req.protocol} ${res.req.method} "/${res.req.path}" with ${res.statusCode} ${res.statusMessage} in ${date.getMilliseconds() - res.req.date.getMilliseconds()}ms`);
    console.log('\n');
  },

  /**
   * Heroku-styled logging
   */
  // finishedProcessing: (httpCode, data) => {
  //   const date = new Date();
  //   console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Router Completed:${colors.Reset} ${colors.Dim}id=${colors.Reset}${colors.FgGreen}${data.id}${colors.Reset} ${colors.Dim}protocol=${colors.Reset}${colors.FgYellow}${data.protocol}${colors.Reset} ${colors.Dim}method=${colors.Reset}${colors.FgYellow}${data.method}${colors.Reset} ${colors.Dim}host=${colors.Reset}${colors.FgYellow}${data.origin}${colors.Reset} ${colors.Dim}endpoint=${colors.Reset}${colors.FgGreen}"/${data.path}"${colors.Reset} ${colors.Dim}status=${colors.Reset}${httpCode >= 200 && httpCode < 300 ? colors.FgGreen : (httpCode >= 301 && httpCode < 400 ? colors.FgYellow : colors.FgRed)}${httpCode}${colors.Reset} ${colors.Dim}elapsed=${colors.Reset}${colors.FgGreen}${date.getMilliseconds() - data.date.getMilliseconds()}ms${colors.Reset}`);
  //   console.log();
  // },

  error: (res: Response, body: Object) => {
    const date = new Date();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} Router Working:${colors.Reset} ${colors.FgWhite}[${res.getHeader('x-request-id')}] Error raised: ${body.message}${colors.Reset}`);
    logToFile(`${logs.logWarningDatesMessages(date)} Router Working: [${res.getHeader('x-request-id')}] Error raised: ${body.message}`);
  },
  mongdodb: (action: string) => {
    const date = new Date();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} MongoDB:${colors.Reset} ${colors.FgWhite}${action}${colors.Reset}`);
    logToFile(`${logs.logWarningDatesMessages(date)} MongoDB: ${action}`);
  },
  dbIndexes: (indexes: Object | Array<Object>) => {
    const date = new Date();
    console.log(`${colors.FgYellow}${environment.getEnv() == 'development' ? logs.logWarningDatesMessages(date) : ''} MongoDB:${colors.Reset} Creating indexes ${JSON.stringify(indexes)}`);
    logToFile(`${logs.logWarningDatesMessages(date)} MongoDB: Creating indexes ${JSON.stringify(indexes)}`);
  }
}

/**
 * Exporting the data
 */

export default logs;