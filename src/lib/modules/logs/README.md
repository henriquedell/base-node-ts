# Logs

This module exposes some useful terminal logging functions, like these:
* `dbConnected()`, logs when the connection to the database is stablished
* `serverStarted(env)`, logs when the server is fully up, showing the port and mode (development, production, ...)
* `entryRequest(requestData)`, logs some useful information about the request that is been processed
* `processingByControler(controller)`, logs which controller was elected to handle the request
* `displayParameters(params)`, in case of a request with a verb different than GET, the params recieved are displayed
* `controllerNotFound()`, if no controller is elegible, this function handle the thrown of the erro
* `finishedProcessing(statusCode, data)`, logs some useful information about the response, like time spent on the processing, the HTTP status, etc
* `error(err)`, logs the error that was raised through the request processing
* `mongdodb(action)`, logs what is running on the database
* `dbIndexes(indexes)`, logs the indexes that'll be set on the DB 