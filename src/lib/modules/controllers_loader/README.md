# Controllers Loader

This module loads all controllers placed inside `app/controllers` and return an object with the names as the key and their URL as the value.