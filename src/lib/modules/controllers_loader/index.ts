import { readdirSync, lstatSync, PathLike } from "fs";
import { join } from "path";

const capitalize = (s: string) => {
  if (typeof s !== 'string') return '';
  let capitalized = '';
  s.split('_').forEach(part => {
    capitalized += part.charAt(0).toUpperCase() + part.slice(1)
  })
  return capitalized;
}

let controllers: Object = {};
const controllersDirectory = join(__dirname, `../../../app/controllers`);

function loadDirectory(controllersDirectory: any, parent: Object = {}) {
  const files = readdirSync(controllersDirectory);
  files.forEach((file, index) => {
    if(lstatSync(`${controllersDirectory}/${file}`).isDirectory()) {
      if(file != 'web') {
        parent[capitalize(file)] = {};
        loadDirectory(join(controllersDirectory, `/${file}`), parent[capitalize(file)]);
      } else {
        loadDirectory(join(controllersDirectory, `/${file}`), parent);
      }
    } else parent[file.replace(/[$\.]ts|[$\.]js/, '').split(/\./g).map(partOfName => capitalize(partOfName)).join('')] = `${controllersDirectory}/${file.replace(/[$\.]ts|[$\.]js/, '')}`;
  });
  return parent;
}

export default function loadControllers() {
  controllers = loadDirectory(controllersDirectory);
  return controllers;
}
