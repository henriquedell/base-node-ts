import { readdirSync, lstatSync, PathLike } from "fs";
import { join } from "path";

const capitalize = (s: string) => {
  if (typeof s !== 'string') return '';
  let capitalized = '';
  s.split('_').forEach(part => {
    capitalized += part.charAt(0).toUpperCase() + part.slice(1)
  })
  return capitalized;
}

let middlewares: Object = {};
const middlewaresDirectory = join(__dirname, `../../../app/middlewares`);

function loadDirectory(middlewaresDirectory: any) {
  const files = readdirSync(middlewaresDirectory);
  files.forEach((file, index) => {
    middlewares[file.replace(/[$\.]ts|[$\.]js/, '').split(/\./g).map(partOfName => capitalize(partOfName)).join('')] = require(`${middlewaresDirectory}/${file.replace(/[$\.]ts|[$\.]js/, '')}`);
  });
}

export default function loadMiddlewares() {
  loadDirectory(middlewaresDirectory);
  return middlewares;
}
