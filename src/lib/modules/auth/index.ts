import secrets from '@config/secrets';
import * as jwt from 'jsonwebtoken';
import application from '@config/application';
import { createClient } from 'redis';
import { Request } from "../classes";

const redis = createClient();

function signIn(user: Object, req: Request) {
  const token = jwt.sign({ id: user.id || user._id }, secrets.JWT_SECRET_KEY, application.jwtSignOptions);
  req.headers['authorization'] = `Bearer ${token}`;
}

function logOut(req: Request) {
  let decoded: string | Object;
  try {
    decoded = jwt.verify((req.headers['authorization'] || 'Bearer ').replace('Bearer ', ''), secrets.JWT_PUBLIC_KEY, application.jwtVerifyOptions);
    redis.set(`${(decoded as Object).id}_${(decoded as Object).iat}`, (req.headers['authorization'] || 'Bearer ').replace('Bearer ', ''));
    const now = new Date();
    const tokenExpirationDate = new Date((decoded as Object).exp * 1000);
    const timeLeft = ((tokenExpirationDate.getTime() - now.getTime())/1000) + 3600;
    redis.expire(`${(decoded as Object).id}_${(decoded as Object).iat}`, timeLeft);
    req.headers['authorization'] = undefined;
  } catch(e) {
    console.log.bind(req)(e);
  } 
  // finally {
  // }
}

function verify(token: string) {
  try {
    return jwt.verify(token, secrets.JWT_PUBLIC_KEY, application.jwtVerifyOptions);
  } catch(e) {
    console.error(e);
    return undefined;
  }
}

function retrieveUserID(this: Request, callback: Function) {
  let decoded: string | Object;
  try {
    decoded = jwt.verify((this.headers['authorization'] || 'Bearer ').replace('Bearer ', ''), secrets.JWT_PUBLIC_KEY, application.jwtVerifyOptions);
    redis.get(`${(decoded as Object).id}_${(decoded as Object).iat}`, (err: any, value: any) => {
      if (err || value) {
        callback(err || 'Token already logged out', null, null);
      } else {
        callback(false, (decoded as Object).id, (user: Object, next: Function = () => {}) => {
          this.currentUser = user;
          next();
        });
      }
    });
  } catch(e) {
    callback(e, null, null);
  }
  // finally {
  // }
}

const auth = {
  signIn,
  logOut,
  verify,
  retrieveUserID
}

export default auth;