declare interface Object {
  [key: string]: any
}

declare interface Global extends NodeJS.Global {
  [x: string]: any;
}

declare const Router: {
  get: (endpoint: string, ...handlers: string[]) => void;
  post: (endpoint: string, ...handlers: string[]) => void;
  put: (endpoint: string, ...handlers: string[]) => void;
  patch: (endpoint: string, ...handlers: string[]) => void;
  delete: (endpoint: string, ...handlers: string[]) => void;
  use: (...middleware: string[]) => void;
  group: (endpoints: Function, ...middlewares: string[]) => void;
  routeExists: (endpoint: string, req: Request) => boolean;
  getRoute: (endpoint: string, req: Request) => Object;

  routes: Object;
}

declare const Requests: {
  get: (url: string, options?: {}) => Promise<{}>;
  post: (url: string, body: Object, options?: {}) => Promise<{}>;
}

declare const Mailer: {
  send: (data: Object, config?: Object) => Promise<{}>; 
}

declare const Helper: {
  hash: (value: string) => Promise<string>;
  generateToken: () => string;
  isObjectEmpty: (object: Object) => boolean;
  stringToISOStringDate: (date: string) => string;
  isValid: (plainText: string, hash: string) => Promise<boolean>;
  UUID: () => string;
}

declare const Auth: {
  signIn: (user: Object, req: Request) => void;
  logOut: (req: Request) => void;
  verify: (token: string) => string | object | undefined;
  retrieveUserID: (this: Request, callback: Function) => void;
}