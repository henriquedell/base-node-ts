import { MongoClient, ObjectId, Db } from 'mongodb';
import * as packageJson from '../../../../package.json';
import { readdirSync } from "fs";
import { join } from "path";
import logs from '../logs';

let db: Db,
    dbName: string,
    isConsole: Boolean,
    url: string = '',
    indexes: Object = {};

const loadedClasses: Object = {};

const models: Object[] = [];
const modelsDirectory = join(__dirname, `../../../app/models`);

function isRunningOnConsole() {
  return isConsole;
}

async function attachValidations(key: string, schema: Object) {
  return new Promise(resolve => {
    if (schema.required.length == 0) delete schema.required;
    db.listCollections().toArray(function(err: any, collections: any) {
      if (collections.map((coll: any) => coll.name).indexOf(key.toString()) > -1) {
        db.command({
          collMod: key.toString(),
          validator: {
            $jsonSchema: schema
          }
        }, function(err: any, results: any) {
          if (err) console.log(err);
          resolve();
        });
      } else {
        db.createCollection(key.toString(), {
          'validator': { 
            $jsonSchema: schema
          }
        }, function(err: any, results: any) {
          if (err) console.log(err);
          resolve();
        });
      }
    })
  });
}

function createValidations(db: Db) {
  return new Promise(async (resolve, reject) => {
    const capitalize = (s: string) => {
      if (typeof s !== 'string') return '';
      return s.charAt(0).toUpperCase() + s.slice(1);
    };
  
    const collections = readdirSync(modelsDirectory);
    collections.forEach((collection, index) => {
      models.push({[capitalize(collection.replace(/[$\.].*/, ''))]: `${modelsDirectory}/${collection.replace(/[$\.].*/, '')}`});
    });

    let promises: Promise<any>[] = [];
    for (let object of models) {
      for (let key of Object.keys(object)) {
        // indexes[key] = [];
        // indexes[key].push('id');
        import(object[key]).then(importedClass => {
          const model = importedClass.default;
          promises.push(attachValidations(key, model.schema.jsonSchema));
        });
        // let modelSchema = {};
        // let properties = {};
        // let required = [];
        // for (let schemaKey of Object.keys(model.schema)) {
        //   if (schemaKey != 'handlers') {
        //     const newPropertie = {};
        //     if (
        //       schemaKey != 'relationships' &&
        //       schemaKey != 'belongsTo'
        //     ) {
        //       newPropertie[schemaKey] = {};
        //       if (typeof(model.schema[schemaKey]) == 'object') {
        //         if (model.schema[schemaKey].unique) {
        //           indexes[key].push(schemaKey)
        //         }
        //         if (new model.schema[schemaKey].type() instanceof Date) {
        //           newPropertie[schemaKey].bsonType = 'date';
        //         } else {
        //           const type = model.schema[schemaKey].type().constructor.name.toLowerCase()
        //           switch (type) {
        //             case 'number':
        //               newPropertie[schemaKey].type = 'number';
        //               break;
        //             case 'boolean':
        //               newPropertie[schemaKey].bsonType = 'bool';
        //               break;
        //             default:
        //               newPropertie[schemaKey].bsonType = type
        //           }
        //         }
        //         for (let key in model.schema[schemaKey]) {
        //           if (key !== 'type' &&
        //               key !== 'default' &&
        //               key !== 'required') {
        //             newPropertie[schemaKey][key] = model.schema[schemaKey][key];
        //           }
        //         }
        //         if (model.schema[schemaKey].required) required.push(schemaKey);
        //       } else if (new model.schema[schemaKey]() instanceof Date) {
        //         newPropertie[schemaKey].bsonType = 'date';
        //       } else {
        //         const type = model.schema[schemaKey]().constructor.name.toLowerCase()
        //         switch (type) {
        //           case 'number':
        //             newPropertie[schemaKey].type = 'number';
        //             break;
        //           case 'boolean':
        //             newPropertie[schemaKey].bsonType = 'bool';
        //             break;
        //           default:
        //             newPropertie[schemaKey].bsonType = type
        //         }
        //       }
        //       properties[schemaKey] = newPropertie[schemaKey];
        //     } else {
        //       switch (schemaKey) {
        //         case 'relationships':
        //           for (const relation of model.schema.relationships) {
        //             if (relation.oneToOne) {
        //               properties[`${relation.model.toLowerCase()}Id`] = { bsonType: 'string' };
        //               if (relation.required) {
        //                 required.push(`${relation.model.toLowerCase()}Id`);
        //               }
        //             } else {
        //               // properties[`${relation.model.toLowerCase()}Ids`] = { bsonType: ['string'] };
        //               properties[`${relation.model.toLowerCase()}Ids`] = { 
        //                 bsonType: 'array',
        //                 uniqueItems: true,
        //                 additionalProperties: false,
        //                 items: {
        //                   bsonType: 'string'
        //                 }
        //               };
        //               if (relation.required) {
        //                 required.push(`${relation.model.toLowerCase()}Ids`);
        //               }
        //             }
        //           }
        //           break;
        //         case 'belongsTo':
        //           for (const belongging of model.schema.belongsTo) {
        //             properties[`${belongging.toLowerCase()}Id`] = { bsonType: 'string' };
        //             let requestedModel = models.filter(model => model[belongging])[0];
        //             let requestedModelRelations = (require(requestedModel[belongging]))[belongging].schema.relationships;
        //             if (
        //               loadedClasses[belongging] &&
        //               loadedClasses[belongging].schema.relationships.filter(relation => relation.model == model.class).length != 0 &&
        //               loadedClasses[belongging].schema.relationships.filter(relation => relation.model == model.class)[0].required
        //             ) required.push(`${belongging.toLowerCase()}Id`);
        //             else if (
        //               requestedModelRelations.filter(relation => relation.model == model.class).length != 0 &&
        //               requestedModelRelations.filter(relation => relation.model == model.class).required
        //             ) required.push(`${belongging.toLowerCase()}Id`);
        //           }
        //           break;
        //       }
        //     }
        //   }
        //   // if (Object.keys(model.schema).indexOf(schemaKey) == Object.keys(model.schema).length - 1) {
        //   // }
        // }
        // properties['id'] = { bsonType: 'objectId' };
        // properties['_id'] = { bsonType: 'objectId' };
        // properties['createdAt'] = { bsonType: 'date' };
        // properties['updatedAt'] = { bsonType: 'date' };
        // indexes[key].push('id');
        // modelSchema = {
        //   bsonType: "object",
        //   additionalProperties: false,
        //   required,
        //   properties,
        // };
      }
    }
    Promise.all(promises).then(() => {
      resolve();
    }, err => console.log(err));
  });
}

function stablishConnection() {
  return new Promise((resolve, reject) => {
    const client = new MongoClient(url, { useUnifiedTopology: true })
    client.connect(function(err) {
      if (err) {
        client.close();
        return reject({name: err.name, message: err.errmsg});
      }

      db = client.db(dbName);
      resolve(client);
    });
  });
}

function connect(mongoURL: string, isOnConsole = false) {
  url = mongoURL;
  isConsole = isOnConsole;
  return new Promise((resolve, reject) => {
    dbName = packageJson.name
    stablishConnection().then(async (client: any) => {
      await createValidations(db).then(_ => {
        if (!isRunningOnConsole()) logs.dbConnected();
        else console.log('Connected to the database');
      }, err => console.log(err));
      if (!isRunningOnConsole()) logs.dbIndexes(indexes);
      for (const index in indexes) {
        for (const key of indexes[index]) {
          await db.collection(key).createIndex({ [key]: 1}, { unique: true }).catch(err => console.log(err));
        }
      }
      client.close();
      resolve();
    }, (err: any) => {
      reject(err);
    });
  });
}

function updateItself(oldMe: Object, newMe: Object) {
  for (const key in newMe) {
    if (newMe[key] instanceof Date) oldMe[key] = new Date(newMe[key]);
    else oldMe[key] = newMe[key];
  }
  delete oldMe['_id'];
}

function updateAllOneToOneBelongsToReferences(object: Object) {
  return new Promise(async resolve => {
    for (const relation of loadedClasses[object.class].schema.modelSchema.belongsTo) {
      if (object[`${relation.toLowerCase()}Id`]) {
        const relationship = loadedClasses[relation].schema.relationships.filter((rel: Object) => rel.model == object.class);
        if (relationship.length == 1 && relationship[0].oneToOne) {
          await db.collection(relation).updateMany(
            { 
              [`${object.class.toLowerCase()}Id`]: object.id.toString(),
              id: { '$ne': new ObjectId(object[`${relation.toLowerCase()}Id`]) }
            },
            { '$set': { [`${object.class.toLowerCase()}Id`]: '' } },
            // { bypassDocumentValidation: true }, Cannot be used here, don't know if it is needed
          ).catch(err => console.log(err))
        }
      }
    }
    resolve();
  })
}

function updateItsOneToOneRelationBelongsTo(object: Object) {
  return new Promise(async (resolve, reject) => {
    for (const relation of loadedClasses[object.class].schema.modelSchema.relationships) {
      if (relation.oneToOne) {
        let result = [];
        if (object[`${relation.model.toLowerCase()}Id`]) {
          result = await loadedClasses[relation.model].where({ id: object[`${relation.model.toLowerCase()}Id`] }, false).catch((err: any) => console.log(err));
        }
        if (result.length > 0) {
          result[0][`${object.class.toLowerCase()}Id`] = object.id.toString();
          await result[0].save().catch((err: any) => console.log(err));
        }
      }
    }
    resolve();
  });
}

function runBeforeSaveHooks(object: Object, hooks: Object[]) {
  return new Promise(async (resolve, reject) => {
    for(let handlersFunc of hooks) {
      try {
        await handlersFunc.bind(object)();
      } catch(err) {
        reject(err);
      }
    }
    resolve();
  });
}

function runModelValidations(object: Object) {
  return new Promise(async (resolve, reject) => {
    for (let attribute in { ...object }) {
      let attributeExistsInClass = false;
      for (let key in { ...loadedClasses[object.class].schema.modelSchema}) {
        if (
          attribute != 'id' &&
          attribute != 'createdAt' &&
          attribute != 'updatedAt'
        ) {
          if (attribute == key) {
            attributeExistsInClass = true;
            if (typeof(loadedClasses[object.class].schema.modelSchema[key]) != 'object') {
              if (object[attribute] instanceof Date) {
                if (!(new loadedClasses[object.class].schema.modelSchema[key]() instanceof Date)) {
                  reject({
                    message: `Attribute "${attribute}" mismatched declared type: ${typeof(loadedClasses[object.class].schema.modelSchema[key]())}`
                  })
                }
              } else if (new loadedClasses[object.class].schema.modelSchema[key]() instanceof Date) {
                reject({
                  message: `Attribute "${attribute}" mismatched declared type: date`
                })
              } else if (typeof(object[attribute]) != typeof(loadedClasses[object.class].schema.modelSchema[key]())) {
                reject({
                  message: `Attribute "${attribute}" mismatched declared type: ${typeof(loadedClasses[object.class].schema.modelSchema[key]())}`
                })
              }
            } else if (object[attribute] instanceof Date) {
              if (!(new loadedClasses[object.class].schema.modelSchema[key].type() instanceof Date)) {
                reject({
                  message: `Attribute "${attribute}" mismatched declared type: ${typeof(loadedClasses[object.class].schema.modelSchema[key].type())}`
                })
              }
            } else if (new loadedClasses[object.class].schema.modelSchema[key].type() instanceof Date) {
              reject({
                message: `Attribute "${attribute}" mismatched declared type: date`
              })
            } else if (typeof(object[attribute]) == typeof(loadedClasses[object.class].schema.modelSchema[key].type())) {
              if (loadedClasses[object.class].schema.modelSchema[key].validation) {
                let invalidReturn;
                try {
                  invalidReturn = await loadedClasses[object.class].schema.modelSchema[key].validation(object[attribute]);
                } catch(err) {
                  reject(err)
                }
                if(invalidReturn) {
                  reject(invalidReturn);
                }
              }
            } else {
              reject({
                message: `Attribute "${attribute}" mismatched declared type: ${typeof(loadedClasses[object.class].schema.modelSchema[key].type())}`
              })
            }
          }
        } else {
          attributeExistsInClass = true;
        }
      }
      if (!attributeExistsInClass) {
        reject({
          message: `Attribute "${attribute}" doesn't exist on class`
        })
      }
    }
    resolve();
  })
}

const mongoStaticsMethods: Object = {
  create: function(object: Object, runValidations: Boolean = true) {
    return new Promise(async (resolve, reject) => {
      if (!(object instanceof loadedClasses[this.class])) object = new loadedClasses[this.class](object);

      /**
       * Convert date strings to type Date
       */
      for (let attribute in { ...object }) {
        if (loadedClasses[this.class].schema.modelSchema[attribute]) {
          if (!(object[attribute] instanceof Date)) {
            if (typeof(loadedClasses[object.class].schema.modelSchema[attribute]) == 'object') {
              if (new loadedClasses[object.class].schema.modelSchema[attribute].type() instanceof Date) object[attribute] = new Date(Helper.stringToISOStringDate(object[attribute]));
            } else {
              if (new loadedClasses[object.class].schema.modelSchema[attribute]() instanceof Date) object[attribute] = new Date(Helper.stringToISOStringDate(object[attribute]));
            }
          }
        }
      }

      /**
       * Model validations
       */
      if (runValidations) {
        try {
          await runModelValidations(object);
        } catch(err) {
          return reject(err);
        }
      }

      /**
       * Model before save hooks, used to validate something or change a value
       * before saving
       */
      if(loadedClasses[object.class].schema.beforeSaveHooks.length > 0) {
        await runBeforeSaveHooks(object, loadedClasses[object.class].schema.beforeSaveHooks);
      }

      stablishConnection().then(async (client: any) => {
        const date = new Date();
        object['createdAt'] = date;
        object['updatedAt'] = date;
        if (!isRunningOnConsole()) logs.mongdodb(`INSERT ${this.class} ${JSON.stringify(object)}`);
        db.collection(this.class).find({ id: new ObjectId(object.id) }).toArray((err, result) => {
          if (err) {
            console.log(err);
            client.close();
            return reject({name: err.name, message: err.errmsg});
          }
          if (result.length == 0) {
            object['_id'] = object.id;
            db.collection(this.class).insertOne(
              object,
              {
                bypassDocumentValidation: !runValidations
              },
              async (err, result) => {
                if (err) {
                  console.log(err)
                  client.close();
                  return reject({name: err.name, message: err.errmsg});
                }

                if (loadedClasses[object.class].schema.modelSchema.relationships) {
                  await updateItsOneToOneRelationBelongsTo(object).catch(err => reject(err));
                }

                client.close();
                updateItself(object, result.ops[0]);
                resolve(true);
              }
            );
          } else {
            return reject('Object already on DB, please use .update() instead');
          }
        });
      });
    });
  },
  delete: function(object: Object) {
    return new Promise(async (resolve, reject) => {
      if (loadedClasses[object.class].schema.modelSchema.relationships) {
        for (const relation of loadedClasses[object.class].schema.modelSchema.relationships) {
          if (relation.oneToOne && relation.dependentDestroy) {
            let result = [];
            if (object[`${relation.model.toLowerCase()}Id`]) {
              result = await loadedClasses[relation.model].where({ id: object[`${relation.model.toLowerCase()}Id`] }, false).catch((err: any) => console.log(err));
            }
            if (result.length > 0) {
              const deleted = await result[0].delete().catch((err: any) => console.log(err));
              if(deleted) object[`${relation.model.toLowerCase()}Id`] = '';
            }
          }
        }
      }

      stablishConnection().then((client: any) => {
        if (!isRunningOnConsole()) logs.mongdodb(`DELETE ${object.class} ${JSON.stringify({ id: object.id })}`);
        db.collection(this.class).deleteOne({ id: new ObjectId(object.id) }, function(err, result) {
          client.close();
          if (err) {
            console.log(err);
            return reject({name: err.name, message: err.errmsg});
          }
          // client.close();
          resolve(true);
        });
      });
    });
  },
  deleteAll: function(array: Object[]) {
    return new Promise((resolve, reject) => {
      stablishConnection().then((client: any) => {
        Promise.all(
          array.map(item => {
            return this.delete(item);
          })
        ).then(_ => {
          client.close();
          resolve(true);
        }, err => {
          client.close();
          reject(err);
        });
      });
    });
  },
  findById: function(id: string, logQuery: Boolean = true) {
    return new Promise((resolve, reject) => {
      if (id) {
        stablishConnection().then((client: any) => {
          if (!isRunningOnConsole() && logQuery) logs.mongdodb(`FIND ONE ${this.class} ${JSON.stringify({ id: new ObjectId(id) })}`);
          db.collection(this.class).find({ id: new ObjectId(id) }).toArray((err, result) => {
            client.close();
            if (err) {
              console.log(err);
              return reject({name: err.name, message: err.errmsg});
            }
            if (result.length > 0) {
              const objs = []
              for(const item of result) {
                delete item['_id'];
                if (item) objs.push(new loadedClasses[this.class](item))
              }
              resolve(objs[0]);
            } else {
              reject('Not found');
            }
          });
        });
      } else {
        resolve(undefined);
      }
    });
  },
  where: function(expression: Object, logQuery: Boolean = true) {
    return new Promise((resolve, reject) => {
      if (expression) {
        stablishConnection().then((client: any) => {
          const parsedExpression: Object = {};
          for (const key in { ...expression }) {
            if (key == "id") parsedExpression["id"] = new ObjectId(expression[key]);
            else parsedExpression[key] = expression[key];
          }
          if (!isRunningOnConsole() && logQuery) logs.mongdodb(`FIND ${this.class} ${JSON.stringify(parsedExpression)}`);
          db.collection(this.class).find(parsedExpression).toArray((err, result) => {
            client.close();
            if (err) {
              console.log(err);
              return reject({name: err.name, message: err.errmsg});
            }
            // client.close();
            const objs = []
            for(const item of result) {
              delete item['_id'];
              if (item) objs.push(new loadedClasses[this.class](item))
            }
            resolve(objs);
          });
        });
      } else {
        resolve([]);
      }
    });
  },
  all: function() {
    return new Promise((resolve, reject) => {
      stablishConnection().then((client: any) => {
        if (!isRunningOnConsole()) logs.mongdodb(`FIND ALL ${this.class}`);
        db.collection(this.class).find().toArray((err, result) => {
          client.close();
          if (err) {
            console.log(err);
            return reject({name: err.name, message: err.errmsg});
          }
          // client.close();
          const objs = []
          for(const item of result) {
            delete item['_id'];
            if (item) objs.push(new loadedClasses[this.class](item))
          }
          resolve(objs);
        });
      });
    });
  },
  anyOf: function(...expressions: Object[]) {
    return new Promise((resolve, reject) => {
      const parsedExpression: Object[] = [];
      for (const object of expressions) {
        const parsedObject: Object = {};
        for (const key in { ...object }) {
          if (object[key]) {
            if (key == "id") parsedObject["id"] = new ObjectId(object[key]);
            else parsedObject[key] = object[key];
          }
        }
        if (!Helper.isObjectEmpty(parsedObject)) {
          parsedExpression.push(parsedObject);
        }
      }
      stablishConnection().then((client: any) => {
        if (!isRunningOnConsole()) logs.mongdodb(`ANY OF ${this.class} ${JSON.stringify(parsedExpression)}`);
        db.collection(this.class).find({ $or: parsedExpression }).toArray((err, result) => {
          client.close();
          if (err) {
            console.log(err);
            return reject({name: err.name, message: err.errmsg});
          }
          // client.close();
          const objs = []
          for(const item of result) {
            delete item['_id'];
            if (item) objs.push(new loadedClasses[this.class](item))
          }
          resolve(objs);
        });
      });
    });
  },
}

const mongoObjectMethods: Object = {
  update: function(attributesToAdd: Object, runValidations: Boolean = true) {
    return new Promise(async (resolve, reject) => {
      /**
       * Model validations
       */
      if (runValidations) {
        try {
          await runModelValidations(new loadedClasses[this.class]({ ...this, ...attributesToAdd}));
        } catch(err) {
          return reject(err);
        }
      }

      /**
       * Model before save hooks, used to validate something or change a value
       * before saving
       */
      if(loadedClasses[this.class].schema.beforeSaveHooks.length > 0) {
        await runBeforeSaveHooks(this, loadedClasses[this.class].schema.beforeSaveHooks);
      }

      stablishConnection().then((client: any) => {
        const updated: Object = attributesToAdd ? { ...attributesToAdd } : { ...this }
        const date = new Date();
        updated['updatedAt'] = date;
        /**
         * Convert date strings to type Date
         */
        for (let attribute in { ...updated }) {
          if (loadedClasses[this.class].schema.modelSchema[attribute]) {
            if (!(updated[attribute] instanceof Date)) {
              if (typeof(loadedClasses[this.class].schema.modelSchema[attribute]) == 'object') {
                if (new loadedClasses[this.class].schema.modelSchema[attribute].type() instanceof Date) updated[attribute] = new Date(Helper.stringToISOStringDate(updated[attribute]));
              } else {
                if (new loadedClasses[this.class].schema.modelSchema[attribute]() instanceof Date) updated[attribute] = new Date(Helper.stringToISOStringDate(updated[attribute]));
              }
            }
          }
        }
        const updatedObjectToLog = { ...updated };
        if (!isRunningOnConsole()) logs.mongdodb(`UPDATE ${this.class} ${JSON.stringify({ id: this.id })} ${JSON.stringify(updatedObjectToLog)}`);
        db.collection(this.class).updateOne(
          { id: new ObjectId(this.id) },
          { '$set': updated },
          {
            bypassDocumentValidation: !runValidations
          },
          async (err, result) => {
            if (err) {
              console.log('The object may not be in DB yet, please use .save() instead\n');
              console.log(err);
              client.close();
              return reject({name: err.name, message: err.errmsg});
            }

            const updatedObj = new loadedClasses[this.class]({ ...this, ...updated });

            if (loadedClasses[this.class].schema.modelSchema.belongsTo) {
              await updateAllOneToOneBelongsToReferences(updatedObj).catch(err => reject(err));
            }

            if (loadedClasses[this.class].schema.modelSchema.relationships) {
              await updateItsOneToOneRelationBelongsTo(updatedObj).catch(err => reject(err));
            }

            client.close();
            await loadedClasses[this.class].where({ id: new ObjectId(this.id) }, false).then((result: any) => {
              updateItself(this, result[0]);
              resolve(true);
            }, (err: any) => {
              reject(err);
            })
          }
        );
      });
    });
  },
  save: function(runValidations: Boolean = true) {
    return new Promise(async (resolve, reject) => {
      await loadedClasses[this.class].where({ id: new ObjectId(this.id) }, false).then(async (result: any) => {
        if (result.length == 0) { // Saving
          let error;
          let result = await loadedClasses[this.class].create(this, runValidations).catch((err: any) => {
            error = err;
          });
          if (error) reject(error);
          else resolve(result);
        } else { // Updating
          let error;
          let result = await this.update({ ...this }, runValidations).catch((err: any) => {
            error = err;
          });
          if (error) reject(error);
          else resolve(result || undefined);
        }
      }, (err: any) => {
        reject(err);
      })
    });
  },
  delete: function() {
    return new Promise(async (resolve, reject) => {
      let error;
      let result = await loadedClasses[this.class].delete(this).catch((err: any) => {
        error = err;
      });
      if (error) reject(error);
      else {
        resolve(result);
      }
    });
  },
}

// exports.Model = function(modelName, model, handlers) {
//   const obj = {};
//   loadedClasses[modelName] = function(params) {
//     for (let attribute in { ...model }) {
//       if (
//         !obj[attribute] &&
//         attribute != 'belongsTo' &&
//         attribute != 'relationships' &&
//         attribute != 'handlers'
//       ) {
//         if (typeof(model[attribute]) == 'function') {
//           this[attribute] = model[attribute]();
//         } else if(typeof(model[attribute]) == 'object') {
//           if (new model[attribute].type() instanceof Date) {
//             this[attribute] = model[attribute].default ? new model[attribute].type(Helper.stringToISOStringDate(model[attribute].default)) : model[attribute].type();
//           } else {
//             this[attribute] = model[attribute].default ? model[attribute].default : model[attribute].type();
//           }
//         } else {
//           this[attribute] = model[attribute];
//         }
//       }
//     }

//     /**
//      * Append id
//      */
//     this.id = ObjectId();

//     /**
//      * Append model relations
//      */
//     if (model.relationships) {
//       for (const relation of model.relationships) {
//         if (!relation.embeddable) {

//           /**
//            * This might be useful on an embeddable scenario
//            */
//           // this[relation.model.toLowerCase()] = relation.oneToOne ? new loadedClasses[relation.model]() : [new loadedClasses[relation.model]()]

//           if (relation.oneToOne) {
//             this[`${relation.model.toLowerCase()}Id`] = '';
  
//           } else {
//             this[`${relation.model.toLowerCase()}Ids`] = [];
//           }
//         }
//       }
//     }

//     /**
//      * Append belonggings
//      */
//     if (model.belongsTo) {
//       for (const relation of model.belongsTo) {
//         this[`${relation.toLowerCase()}Id`] = '';
//       }
//     }

//     /**
//      * Append some Mongo helpers
//      */
//     for (const attribute in { ...mongoObjectMethods }) {
//       if (!obj[attribute]) {
//         Object.defineProperty(this, attribute, {
//           enumerable: false,
//           configurable: false,
//           writable: false,
//           value: mongoObjectMethods[attribute]
//         });
//       }
//     }

//     /**
//      * The constructor params
//      * when instantiating the object
//      */
//     if (!params) params = {};
//     for(const param in { ...params }) {
//       if (param != "id") this[param] = params[param];
//       else this[param] = ObjectId(params[param]);
//     }

//     /**
//      * Append the model handlres
//      */
//     Object.defineProperty(this, 'handlers', {
//       enumerable: false,
//       configurable: false,
//       writable: false,
//       value: handlers
//     });

//     /**
//      * Setting the object's class
//      */
//     Object.defineProperty(this, 'class', {
//       enumerable: false,
//       configurable: false,
//       writable: false,
//       value: modelName
//     });
//   }

//   /**
//    * Append the model validations
//    */
//   loadedClasses[modelName].validations = {};
//   if (model.validations) {
//     loadedClasses[modelName].validations = model.validations
//   }

//   /**
//    * Append some Mongo helpers
//    */
//   for (const attribute in { ...mongoStaticsMethods }) {
//     if (!obj[attribute]) {
//       loadedClasses[modelName][attribute] = mongoStaticsMethods[attribute];
//     }
//   }

//   /**
//    * Add the class name
//    */
//   loadedClasses[modelName]['class'] = modelName;

//   /**
//    * Append the model schema
//    */
//   loadedClasses[modelName].schema = model;

//   return loadedClasses[modelName];
// }

function Schema(schema: Object) {
  let properties: Object = {};
  let required: string[] = [];
  for (let schemaKey of Object.keys(schema)) {
    if (
      schemaKey != 'prototype' &&
      schemaKey != 'statics'
    ) {
      const newPropertie: Object = {};
      if (
        schemaKey != 'relationships' &&
        schemaKey != 'belongsTo'
      ) {
        newPropertie[schemaKey] = {};
        if (typeof(schema[schemaKey]) == 'object') {
          // if (schema[schemaKey].unique) {
          //   indexes[key].push(schemaKey);
          // }
          if (new schema[schemaKey].type() instanceof Date) {
            newPropertie[schemaKey].bsonType = 'date';
          } else {
            // const type = schema[schemaKey].type().constructor.name.toLowerCase();
            const type = typeof(schema[schemaKey].type());
            switch (type) {
              case 'number':
                newPropertie[schemaKey].type = 'number';
                break;
              case 'boolean':
                newPropertie[schemaKey].bsonType = 'bool';
                break;
              default:
                newPropertie[schemaKey].bsonType = type;
            }
          }
          for (let key in schema[schemaKey]) {
            if (
                key !== 'type' &&
                key !== 'default' &&
                key !== 'required' &&
                key !== 'validation'
              ) {
              newPropertie[schemaKey][key] = schema[schemaKey][key];
            }
          }
          if (schema[schemaKey].required) required.push(schemaKey);
        } else if (new schema[schemaKey]() instanceof Date) {
          newPropertie[schemaKey].bsonType = 'date';
        } else {
          // const type = schema[schemaKey]().constructor.name.toLowerCase();
          const type = typeof(schema[schemaKey]());
          switch (type) {
            case 'number':
              newPropertie[schemaKey].type = 'number';
              break;
            case 'boolean':
              newPropertie[schemaKey].bsonType = 'bool';
              break;
            default:
              newPropertie[schemaKey].bsonType = type;
          }
        }
        properties[schemaKey] = newPropertie[schemaKey];
      } else {
        switch (schemaKey) {
          case 'relationships':
            for (const relation of schema.relationships) {
              if (relation.oneToOne) {
                properties[`${relation.model.toLowerCase()}Id`] = { bsonType: 'string' };
                if (relation.required) {
                  required.push(`${relation.model.toLowerCase()}Id`);
                }
              } else {
                // properties[`${relation.model.toLowerCase()}Ids`] = { bsonType: ['string'] };
                properties[`${relation.model.toLowerCase()}Ids`] = { 
                  bsonType: 'array',
                  uniqueItems: true,
                  additionalProperties: false,
                  items: {
                    bsonType: 'string'
                  }
                };
                if (relation.required) {
                  required.push(`${relation.model.toLowerCase()}Ids`);
                }
              }
            }
            break;
          case 'belongsTo':
            for (const belongging of schema.belongsTo) {
              properties[`${belongging.toLowerCase()}Id`] = { bsonType: 'string' };
              let requestedModel = models.filter(model => model[belongging])[0];
              let requestedModelRelations;
              import(requestedModel[belongging]).then(importedClass => {
                requestedModelRelations = importedClass[belongging].schema.modelSchema.relationships
              })

              // if (
              //   loadedClasses[belongging] &&
              //   loadedClasses[belongging].schema.modelSchema.relationships.filter(relation => relation.model == model.class).length != 0 &&
              //   loadedClasses[belongging].schema.modelSchema.relationships.filter(relation => relation.model == model.class)[0].required
              // ) required.push(`${belongging.toLowerCase()}Id`);
              // else if (
              //   requestedModelRelations.filter(relation => relation.model == model.class).length != 0 &&
              //   requestedModelRelations.filter(relation => relation.model == model.class).required
              // ) required.push(`${belongging.toLowerCase()}Id`);
            }
            break;
        }
      }
    }
  }
  properties['id'] = { bsonType: 'objectId' };
  properties['_id'] = { bsonType: 'objectId' };
  properties['createdAt'] = { bsonType: 'date' };
  properties['updatedAt'] = { bsonType: 'date' };
  const jsonSchema: Object = {
    bsonType: "object",
    additionalProperties: false,
    required,
    properties,
  };

  const beforeSaveHooks: Function[] = [];
  function beforeSave(callback: Function) {
    beforeSaveHooks.push(callback);
  }

  if(Object.keys(properties).filter(key => key == 'password').length > 0) beforeSave(function(this: Object) {
    return new Promise(async (resolve, reject) => {
      this.password = await Helper.hash(this.password).catch((err: any) => reject(err));
      resolve();
    });
  });

  const newSchema: Object = {
    prototype: {},
    static: {},
    beforeSave,
    beforeSaveHooks,
    modelSchema: schema,
    jsonSchema
  };

  return newSchema;
}

function Model(modelName: string, schema: Object) {
  const modelStructure = schema.modelSchema;
  loadedClasses[modelName] = function(params: Object) {
    for (let attribute in { ...modelStructure }) {
      if (
        attribute != 'belongsTo' &&
        attribute != 'relationships' &&
        attribute != 'prototype' &&
        attribute != 'static'
      ) {
        if (typeof(modelStructure[attribute]) == 'function') {
          this[attribute] = modelStructure[attribute]();
        } else if(typeof(modelStructure[attribute]) == 'object') {
          if (new modelStructure[attribute].type() instanceof Date) {
            this[attribute] = modelStructure[attribute].default ? new modelStructure[attribute].type(Helper.stringToISOStringDate(modelStructure[attribute].default)) : modelStructure[attribute].type();
          } else {
            this[attribute] = modelStructure[attribute].default ? modelStructure[attribute].default : modelStructure[attribute].type();
          }
        } else {
          this[attribute] = modelStructure[attribute];
        }
      }
    }

    /**
     * Append id
     */
    this.id = new ObjectId();

    /**
     * Append model relations
     */
    if (modelStructure.relationships) {
      for (const relation of modelStructure.relationships) {
        if (!relation.embeddable) {

          /**
           * This might be useful on an embeddable scenario
           */
          // this[relation.modelStructure.toLowerCase()] = relation.oneToOne ? new loadedClasses[relation.modelStructure]() : [new loadedClasses[relation.modelStructure]()]

          if (relation.oneToOne) {
            this[`${relation.model.toLowerCase()}Id`] = '';
  
          } else {
            this[`${relation.model.toLowerCase()}Ids`] = [];
          }
        }
      }
    }

    /**
     * Append belonggings
     */
    if (modelStructure.belongsTo) {
      for (const relation of modelStructure.belongsTo) {
        this[`${relation.toLowerCase()}Id`] = '';
      }
    }

    /**
     * Append some Mongo helpers
     */
    for (const attribute in { ...mongoObjectMethods }) {
      Object.defineProperty(this, attribute, {
        enumerable: false,
        configurable: false,
        writable: false,
        value: mongoObjectMethods[attribute]
      });
    }
    
    /**
     * Append some useful functions
     */
    Object.defineProperty(this, 'isValidPassword', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: async (password: string) => {
        return await Helper.isValid(password, this.password);
      }
    });

    /**
     * The constructor params
     * when instantiating the object
     */
    if (!params) params = {};
    for(const param in { ...params }) {
      if (param != "id") this[param] = params[param];
      else this[param] = new ObjectId(params[param]);
    }

    /**
     * Append the model methods
     */
    for(const method in schema.prototype) {
      Object.defineProperty(this, method, {
        enumerable: false,
        configurable: false,
        writable: false,
        value: schema.prototype[method]
      });
    }

    /**
     * Setting the object's class
     */
    Object.defineProperty(this, 'class', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: modelName
    });
  }

  /**
   * Append some Mongo helpers
   */
  for (const attribute in { ...mongoStaticsMethods }) {
    loadedClasses[modelName][attribute] = mongoStaticsMethods[attribute];
  }

  /**
     * Append the model methods
     */
    for(const method in schema.static) {
      Object.defineProperty(loadedClasses[modelName], method, {
        enumerable: false,
        configurable: false,
        writable: false,
        value: schema.static[method]
      });
    }

  /**
   * Add the class name
   */
  loadedClasses[modelName]['class'] = modelName;

  /**
   * Append the model schema
   */
  loadedClasses[modelName].schema = schema;

  indexes[modelName] = [];
  indexes[modelName].push('id');

  return loadedClasses[modelName];
}

export {
  connect,
  Schema,
  Model
}