# Database

This module handles the connection to the database. It loads all models placed inside app/models and create the database validations.

It exposes the `dbConnect(url, isOnConsole)`, that connects to the database specified by the url and set the isOnConsole boolean that changes the behavior of some logging made by the other functions on this module.
Shows little less info if `isOnConsole` is `true`.

This module also implements a lot of useful functions to managing data on the DB:
* `Model.create(object, runValidations = true)`, 