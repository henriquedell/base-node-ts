import Server from '@base/server';
import * as database from '@base/database';
import db from '@config/db';

import '@app/routes/api';
import '@app/routes/web';

const server = new Server();
server.listen();

database.connect(db.url)
  .catch(err => {
    console.log(err);
  })