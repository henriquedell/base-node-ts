/**
 * Authentication endpoints
 */
Router.post('/api/auth', 'Auth.login');

// Routing list
Router.get('/api', 'User.getHello');
Router.delete('/api/usuarios', 'User.deleteAll');
Router.get('/api/usuarios/:id', 'User.getUser');
Router.post('/api/usuarios/novo', 'User.new');
Router.put('/api/usuarios/:id', 'User.update');
Router.delete('/api/usuarios/:id', 'User.delete');
Router.get('/api/usuarios', 'User.index');

Router.group(() => {
  Router.get('/api/perfil', 'User.profile');
  Router.delete('/api/auth', 'Auth.logout');
}, 'CheckLogged');