import User from '@app/models/user';
import { Request, Response } from '@base/classes';

export default {
  login(req: Request, res: Response) {
    User.anyOf({cpf: req.params.cpf}, {cpfPlain: req.params.cpf}).then((users: any[]) => {
      if(users.length > 0) {
        const user = users[0];
        user.isValidPassword(req.params.password).then((valid: boolean) => {
          if (valid) {
            req.login(user, (err: any) => {
              if (err) {
                console.log(err);
                return res.status(400).json({ message: 'Não foi possível realizar o login' });
              }
              res.json(user.map());
            });
          } else {
            res.status(400).json({ message: 'Credenciais incorretas' });
          }
        })
      } else {
        res.status(400).json({ message: 'Credenciais incorretas' });
      }
    })
  },

  logout(req: Request, res: Response) {
    req.logout();
    return res.json({ message: 'Successfully logged out' });
  }
}