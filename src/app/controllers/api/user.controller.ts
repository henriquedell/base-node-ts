// import requests from '@base/requests';
import { stringToISOStringDate } from '@base/helpers';
import User from '../../models/user';
// import Address from '../../models/address';
// import Phone from '../../models/phone';
import mailer from '@helpers/mailers/application.mailer';

export default {

  profile(req: any, res: any) {
    res.json(req.currentUser.map());
  },

  index(req: any, res: any) {
    User.all().then((users: Object[]) => {
      res.json(User.mapUsers(users));
    });
  },

  async getUser(req: any, res: any) {
    const user = await User.where({ id: req.urlParams.id });
    if (user.length > 0) {
      res.json(await user[0].map());
    } else {
      res.status(404).json({ message: 'Usuário não encontrado' });
    }
  },

  async new(req: any, res: any) {
    const u = new User(req.params);
    // const u = (await User.where({ id: '5e8ca2358d0aa50a560aa399' }))[0];
    // const address = new Address({
    //   street: 'Av. José Maria da Silva Paranhos',
    //   number: 205
    // });
    // const address = (await Address.where({ id: '5eb87e3cb5d8b92722940d21' }))[0];
    // u.addressId = address.id.toString();
    // address.userId = u.id;
    // console.log(u)

    // const phone = new Phone({
    //   phone: '(11) 9 9935-7345'
    // });
    // address.phoneId = phone.id
    // console.log(phone);
    try {
      // await address.save();
      await u.save();
      // await address.update({ userId: u.id });
      // await phone.save();
      mailer.signup(u);
      res.json(u.map());
    } catch(e) {
      let message;
      if (e.message) message = e.message;
      else message = 'Não foi possível salvar o usuário no momento';
      res.status(400).json({ message });
    } 
    // finally {
    //   mailer.signup(u);
    // }
  },

  async update(req: any, res: any) {
    const u = await User.where({ id: req.urlParams.id });
    if (u.length > 0) {
      try {
        await u[0].update(req.params);
        res.json(await User.mapUsers(u));
      } catch(e) {
        res.status(400).json({ message: 'Não foi possível atualizar o usuário no momento' });
      }
    } else {
      res.status(404).json({ message: 'Usuário não encontrado' });
    }
  },
        
  async delete(req: any, res: any) {
    const u = await User.where({ id: req.urlParams.id });
    if (u.length > 0) {
      try {
        await u[0].delete();
        res.json(u);
      } catch(e) {
        res.status(400).json({ message: 'Não foi possível deletar esse usuário no momento' });
      }
    } else {
      res.status(404).json({ message: 'Usuário não encontrado' });
    }
  },

  deleteAll(req: any, res: any) {
    User.all().then((users: any) => User.deleteAll(users).then(() => res.json()));
  },
        
  getHello(req: any, res: any) {
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    Requests.get('http://localhost:3000/api/usuarios');
    return {
      status: 200,
      response: {}
    };

  },

  // async getHello(req, res) {
  //   await Promise.all([
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //     Requests.get('http://localhost:3000/api/usuarios'),  
  //   ]).then(_ => res.json());
  // },
};