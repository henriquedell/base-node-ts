import { Schema, Model } from '@base/database';
// const Address = require('./address');

const UserSchema = Schema({
  name: String,
  cpf: {
    type: String,
    required: true,
    // unique: true
  },
  cpfPlain: String,
  birthDate: {
    type: Date,
    default: '29/12/1996'
  },
  // year: {
  //   type: Number,
  //   minimum: 2017,
  //   maximum: 2030,
  //   required: true
  // },
  income: Number,
  active: {
    type: Boolean,
    default: true
  },
  password: {
    type: String,
    validation: async function(value: string) {
      if(value.length < 8) return { message: 'Senha precisa ter no mínimo 8 caracteres' };
    }
  },
  email: String

  // relationships: [
  //   {
  //     model: 'Address',
  //     embeddable: false,
  //     oneToOne: true,
  //     dependentDestroy: true,
  //     // required: true
  //   },
  //   {
  //     model: 'Phone',
  //     embeddable: false,
  //     oneToMany: true
  //   }
  // ]
});

UserSchema.beforeSave(function(this: Object) {
  return new Promise(resolve => {
    this.cpfPlain = this.cpf.replace(/-|\.*/g, '');
    resolve();
  });
});

UserSchema.prototype.map = function() {
  const user = {
    id: this.id,
    name: this.name,
    cpf: this.cpf,
    cpfPlain: this.cpfPlain,
    birthDate: this.birthDate,
    income: this.income,
    active: this.active,
    email: this.email,
    address: {},
    // address: this.addressId ? Address.findById(this.addressId) : {},
    phones: []
  }
  return user;
}

UserSchema.static.mapUsers = function(array: Object[]) { 
  // return Promise.all(array.map(item => item.map()));
  return array.map(item => item.map());
}

export default Model('User', UserSchema);