import { Model, Schema } from '@base/database';

const PhoneSchema = Schema({
  phone: String,

  // belongsTo: [
  //   'User'
  // ],

  // relationships: [
  //   {
  //     model: 'Address',
  //     embeddable: false,
  //     oneToOne: true,
  //   },
  // ]
});

export default Model('Phone', PhoneSchema);