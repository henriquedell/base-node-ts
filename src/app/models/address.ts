import { Model, Schema } from '@base/database';

const AddressSchema = Schema({
  street: String,
  number: Number,

  // belongsTo: [
  //   'User',
  //   'Phone'
  // ]
});

export default Model('Address', AddressSchema);