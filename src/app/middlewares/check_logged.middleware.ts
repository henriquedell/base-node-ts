import { Request, Response } from '@base/classes';
import User from '@app/models/user';

export default function(req: Request, res: Response, next: Function) {
  req.retrieveUserID((err: any, id: string, done: Function) => {
    if(!err) {
      User.findById(id)
      .then((user: any) => done(user, next))
      .catch((err: any) => {
        console.log(err)
        res.status(404).json({ message: 'Usuário não encontrado' });
      });
    } else {
      res.status(401).json({ message: 'Por favor, faça o login' });
    }
  });
}