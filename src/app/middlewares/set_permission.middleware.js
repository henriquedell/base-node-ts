exports.setPermission = function(req, res) {
  if (req.currentUser.type == 'Admin') {
    req.currentUser.access = 'full';
  } else {
    req.currentUser.access = 'simple';
  }
}