import * as repl from 'repl';
import * as database from '@base/database';
import * as fileSystem from 'fs';
import * as path from 'path';
import db from '@config/db';

import '@base/globals';

async function startConsole() {
  /**
     * This code will automatically load all models placed in app/models folder
     * and all controller placed inside app/controllers
     */
  const models: Object[] = [];
  const modelsDirectory = path.join(__dirname, 'app/models');
  const capitalize = (s: string) => {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
  }

  await fileSystem.readdir(modelsDirectory, (err, files) => {
    if(err) {
      return console.error(`Unable to read ${modelsDirectory} folder.`);
    }
    files.forEach((file, index) => {
      models.push({
        model: file.replace(/[$\.].*/, '').split(/_/g).map(partOfName => capitalize(partOfName)).join(''),
        path: `${modelsDirectory}/${file}`
      })
    })
  })

  const historyFile = path.join(__dirname, 'lib/.console_history');
  let history: any;

  if(fileSystem.existsSync(historyFile)) {
    const { birthtime } = fileSystem.statSync(historyFile);
    const now = new Date();

    history = fileSystem.readFileSync(historyFile);
  
    // 3600000 milliseconds = 1 hour
    const day = 5 * 3600000;
    if (now.getTime() - birthtime.getTime() > day) {
      fileSystem.unlinkSync(historyFile);
    }
  }

  /**
   * The connection to the database is created
   * and the REPL server is started
   */
  await database.connect(db.url, true).then(_ => {
    console.log('quit(), exit() or Ctrl+C to quit the application');
    console.log('.editor to enter Editor mode. Useful to treat the console more like an IDE');
    console.log();
    
    const replServer: any = repl.start({
      prompt: 'DBConsole > ',
    });

    if(history) history.toString()
    .split('\n')
    .reverse()
    .filter((line: string) => line.trim())
    .map((line: string) => replServer.history.push(line));

    
    models.forEach(model => {
      import(model.path).then(importedModel => {
        replServer.context[`${model.model}`] = importedModel.default;
      })
    });

    function endConsole() {
      fileSystem.appendFileSync(historyFile, `\n${replServer.lines.join('\n')}`);
      console.log('Bye-bye!');
      process.exit();
    }

    replServer.context.exit = () => {
      replServer.lines.push('exit()');
      endConsole();
    };
    replServer.context.quit = () => {
      replServer.lines.push('quit()');
      endConsole();
    };
  }).catch((err) => {
    console.log('\n');
    console.error(`${err}`);
    process.exit(1);
  });
}

startConsole()